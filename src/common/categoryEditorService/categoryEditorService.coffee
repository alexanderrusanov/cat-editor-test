class CategoryEditorService
  constructor: (@store)->
    @propId = 0
    @properties = @store.get('props', [])
    @propertyTypes = ['text', 'number', 'range', 'date', 'time', 'yesno', 'multiplechoices', 'singlechoice']
    @defaultPropertyType = 0
    @propertyTypesAdditionals =
      'number':
        units: [null, '"', 'g', 'mm'] #Add needed units here
      'range':
        units: [null, '"', 'g', 'mm'] #Add needed units here

  addNewProperty: ()->
    @properties.insert(0,
      new CategoryProperty(++@propId, "property#{@propId}", @propertyTypes[@defaultPropertyType],"Property #{@propId} title")
    )

  getPropertyTypes: ()->
    @propertyTypes

  getPropeties: ()->
    @properties

  remove: (property)->
    property.isRemoved = true

  getAdditionalsForType: (type)->
    @propertyTypesAdditionals[type]

  saveAll: ()->
    exportedProperties = []
    #Do some formating with properties
    for property in @properties
      if not property.isRemoved
        exportedProperty = _.chain(property)
                          .pick('id', 'name', 'type', 'required','title','isRemoved')
                          .value()
        exportedProperty[property.type] = property[property.type]
      else
        exportedProperty=
          id: property.id
          removed: true
      exportedProperties.push(exportedProperty)
    #alert with properties for now
    alert(angular.toJson(exportedProperties, true))
    #remove all removed
    @properties = _.filter(@properties,(prop)->!prop.isRemoved)
    #save properties
    @store.set('props', @properties)


angular.module('app.categoryEditorService', ['storageService'])
  .factory("categoryEditorService", (storageService)->
    return new CategoryEditorService(storageService)
  )
