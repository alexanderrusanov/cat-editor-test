appCategoryEditorModule = angular.module('app.category_editor', [
  'ui.state',
  'titleService',
  'app.categoryEditorService'
])

appCategoryEditorModule.config ($stateProvider)->
  $stateProvider.state 'category_editor',
    url: '/category_editor',
    views:
      "main":
        controller: 'CategoryEditorCtrl',
        templateUrl: 'category_editor/category_editor.tpl.html'


appCategoryEditorModule.controller 'CategoryEditorCtrl',
($scope,$dialog, categoryEditorService)->
  $scope.preview = {}
  $scope.showPreview = ()->
    if $scope.previewForm.$valid
      alert(angular.toJson($scope.preview,true))
    else
      errors={}
      for own err,val of $scope.previewForm.$error
        errors[err]=[]
        for errVal in val
          errors[err].push(errVal.$name+" is #{err}")
      alert(angular.toJson(errors,true))


  $scope.properties = {}
  $scope.properties.items = ()-> categoryEditorService.getPropeties()
  $scope.properties.propertyTypes = ()-> categoryEditorService.getPropertyTypes()
  $scope.properties.additionalsForType = (type)-> categoryEditorService.getAdditionalsForType(type)

  $scope.properties.addNewProperty = ()->
    categoryEditorService.addNewProperty()
    $scope.propertiesForm.$setDirty()

  $scope.properties.saveAll = ()->
    if not $scope.propertiesForm.$valid
      alert('some property is not valid')
    else
      categoryEditorService.saveAll()
      $scope.propertiesForm.$setPristine()

  $scope.properties.remove = (property)->
    categoryEditorService.remove(property)
    $scope.propertiesForm.$setDirty()

  ###
  Detect changes in form editors
  ###
  $scope.hasChanges = ()->
    $scope.propertiesForm.$dirty
