appModule = angular.module('app', [
  'templates-app',
  'templates-common',
  #App modules
  'app.category_editor',
  'app.categoryEditorService',

  'ui.state',
  'ui.route',
  'ui.bootstrap',
  'tags-input',
  'storageService'
])

appModule.config ($stateProvider, $urlRouterProvider)->
  $urlRouterProvider.otherwise('/category_editor')

appModule.run (titleService)->
  titleService.setSuffix(' | app')

appModule.controller 'AppCtrl', ($scope, $location)->
  null
